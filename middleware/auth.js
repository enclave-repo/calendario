const jwt = require('../libraries/jwt');

module.exports = function (req, res, next) {
  const token = req.header('x-auth-token');

  if (!token)
    return res.status(401).json({ msg: 'No token, authorization denied' });

  try {
    const decoded = jwt.decode(token);
    req.user = decoded;

    return next();
  } catch (error) {
    res.status(401).json({ msg: 'Token is not valid' });
  }
};
