'use strict';

const bcrypt = require('bcrypt');
const jwt = require('../libraries/jwt');

function Users(db) {
  this.collection = 'Users';
  this.db = db[this.collection];
}

Users.prototype.save = async function (body) {
  if (!('name' in body) || !body.name || body.name.length === 0)
    return { error: 'Es necesario un nombre' };

  if (!('password' in body) || !body.password || body.password.length === 0)
    return { error: 'Es necesaria una contraseña' };

  if (body.name.length < 4 || body.name.length > 15)
    return { error: 'El nombre debe tener entre 4 y 45 letras' };

  if (body.password.length < 8 || body.password.length > 20)
    return { error: 'La contraseña debe tener entre 8 y 20 carateres' };

  let users = await this.db.find({});
  users = users.filter((user) => user.name.toLowerCase() === body.name.toLowerCase());
  if (users.length > 0) return { error: 'Ya existe un usuario con ese nombre' };

  return this.db.insert({ ...body, password: bcrypt.hashSync(body.password, 10) });
};

Users.prototype.login = async function (creds) {
  if (!('name' in creds) || !creds.name || creds.name.length === 0)
    return { error: 'Escribe tu nombre de usuario' };

  if (!('password' in creds) || !creds.password || creds.password.length === 0)
    return { error: 'Escribe tu contraseña' };

  const user = await this.db.findOne({ name: creds.name });

  if (!user) return { error: 'Usuario no encontrado' };
  if (!bcrypt.compareSync(creds.password, user.password)) return { error: 'Contraseña incorrecta' };

  delete user.password;

  return { token: jwt.set(user) };
};

Users.prototype.auth = async function (userId) {
  const user = await this.db.findOne({ _id: userId });

  if (!user) return { error: 'Usuario no encontrado' };
  delete user.password;

  return user;
};

Users.prototype.get = function (filters) {
  return this.db.find(filters ?? {});
};

Users.prototype.getOne = function (filters) {
  return this.db.findOne(filters);
};

module.exports = Users;
