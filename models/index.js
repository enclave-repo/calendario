'use strict';

const fs = require('fs');
const path = require('path');

let models = [];

fs.readdirSync(__dirname).forEach((file) => {
  if (file.includes(path.basename(__filename))) return;

  const fileName = path.basename(file, path.extname(file));

  try {
    models[fileName] = require('./' + fileName);
  } catch (error) {
    console.error(error);
  }
});

module.exports = models;
