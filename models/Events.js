'use strict';

const dayjs = require('dayjs');

function Events(db) {
  this.collection = 'Events';
  this.db = db[this.collection];
}

Events.prototype.save = async function (body) {
  if (!('user' in body) || !body.user || body.user.length === 0) return { error: 'Unauthorized' };

  if (!('name' in body) || !body.name || body.name.length === 0)
    return { error: 'Es necesario un nombre' };

  if (!('date' in body) || !body.date || body.date.length === 0)
    return { error: 'Es necesaria una fecha y hora' };

  if (!('type' in body) || !body.type || body.type.length === 0)
    return { error: 'Un tipo debe ser seleccionado' };

  if (body.name.length < 4 || body.name.length > 45)
    return { error: 'El nombre debe tener entre 4 y 45 letras' };

  const eventDate = dayjs(body.date);

  const events = await this.db.findOne({
    date: {
      $gte: eventDate.subtract(90, 'minute').unix() * 1000,
      $lte: eventDate.add(90, 'minute').unix() * 1000
    }
  });

  if (events !== null) return { error: 'Ya existe algo agendado para esa fecha y hora' };

  return this.db.insert({ ...body, date: eventDate.unix() * 1000 });
};

Events.prototype.get = function (filters) {
  return this.db.find(filters);
};

Events.prototype.delete = function (_id) {
  return this.db.remove({ _id });
};

module.exports = Events;
