import React from 'react';
import ReactDOM from 'react-dom';
import { QueryClient, QueryClientProvider } from 'react-query';

import './index.css';
import App from './App';

const qc = new QueryClient({ defaultOptions: { queries: { retry: false } } });

if (window.location !== window.parent.location)
    ReactDOM.render(
      <React.StrictMode>
        <QueryClientProvider client={qc}>
          <App />
        </QueryClientProvider>
      </React.StrictMode>,
      document.getElementById('enclaveCalendar')
    );
