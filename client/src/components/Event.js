import { useMutation, useQueryClient } from 'react-query';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';

import axios from 'axios';
import dayjs from 'dayjs';
import { useEffect, useState } from 'react';

const Event = ({ event, user }) => {
  const [errorMsg, setErrorMsg] = useState(null);

  const qc = useQueryClient();
  const { mutate, isError, error } = useMutation((id) => axios.delete('/_back/api/events/' + id), {
    onSuccess: () => qc.invalidateQueries('events')
  });

  useEffect(() => {
    if (isError) setErrorMsg(error.response.data);
    else setErrorMsg(null);
  }, [isError, error]);

  return (
    <div className={'columns ctime ' + event.type}>
      <div className='col-9'>
        {`${dayjs(event.date).format('HH:mm')} | ${event.name}`}
        <br />
        <small>({event.user})</small>
      </div>
      <div className='col-1'>
        {user && process.env.REACT_APP_ADMINS.split(',').includes(user) && (
          <button className='btn' onClick={() => mutate(event._id)}>
            <FontAwesomeIcon icon={faTrashAlt} />
          </button>
        )}
      </div>
      {errorMsg && <span className='danger'>{errorMsg}</span>}
    </div>
  );
};

export default Event;
