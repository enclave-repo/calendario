import dayjs from 'dayjs';
import 'dayjs/locale/es';

import { Day } from '.';

dayjs.locale('es');

const Week = ({ now, events, user }) => (
  <div className="columns">
    {[...Array(7).keys()]
      .map((index) =>
        events.filter(
          (event) => dayjs(event.date).$D === now.add(index, 'days').$D
        )
      )
      .map((dayEvents, index) => (
        <Day
          key={index}
          day={now.add(index, 'days')}
          events={dayEvents}
          user={user}
        />
      ))}
  </div>
);

export default Week;
