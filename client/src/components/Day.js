import { Event } from '.';

const Day = ({ day, events, user }) => {
  const dayStr = day.format('dddd');

  return (
    <div className="col day">
      <hr />
      <span className="cdate">
        <h4>
          {dayStr[0].toUpperCase() +
            dayStr.slice(1) +
            ' ' +
            day.$D +
            '-' +
            day.format('MMM')}
        </h4>
      </span>
      {events.map((event, index) => (
        <Event event={event} key={index} user={user} />
      ))}
    </div>
  );
};

export default Day;
