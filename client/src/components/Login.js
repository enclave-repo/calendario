import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useMutation } from 'react-query';

import axios from 'axios';

const Login = () => {
  const [formData, setData] = useState({ name: '', password: '' });
  const [responseMessage, setResponseMessage] = useState('');
  const [spanResponseClass, setSpanResponseClass] = useState('');

  const history = useHistory();

  const { name, password } = formData;

  const { isLoading, mutate, error, isSuccess, data } = useMutation((user) =>
    axios.post('/_back/api/users/login', user)
  );

  const onChange = (e) =>
    setData(() => ({ ...formData, [e.target.name]: e.target.value }));

  const onSubmit = (e) => {
    e.preventDefault();
    mutate({ name, password });
  };

  useEffect(() => {
    if (!isLoading) {
      setResponseMessage(error?.response.data ?? '');
      setSpanResponseClass('warning');
    }
  }, [isLoading, error?.response.data]);

  useEffect(() => {
    if (isSuccess) {
      setResponseMessage('Logeado. Redirigiendo al calendario...');
      setSpanResponseClass('success');
      const { token } = data.data;

      localStorage.setItem('_cal_token', token);
      axios.defaults.headers.common['x-auth-token'] = token;
      setTimeout(() => {
        history.push('/');
      }, 3000);
    }
  }, [isSuccess, data, history]);

  return (
    <div className="container">
      <form className="form centered" onSubmit={(e) => onSubmit(e)}>
        <div className="form-group">
          <div className="columns">
            <div className="col-2">
              <h4>Username</h4>
            </div>
            <div className="col-8">
              <input type="text" name="name" value={name} onChange={onChange} />
            </div>
          </div>
        </div>

        <div className="form-group">
          <div className="columns">
            <div className="col-2">
              <h4>Password</h4>
            </div>
            <div className="col-8">
              <input
                type="password"
                name="password"
                value={password}
                onChange={onChange}
              />
            </div>
          </div>
        </div>

        <div className="columns">
          <div className="col-2"></div>
          <div className="col-8">
            <span className={spanResponseClass}>{responseMessage}</span>
          </div>
        </div>

        <div className="form-group">
          <div className="columns">
            <div className="col">
              <button
                type="button"
                className="btn"
                onClick={() => history.push('/')}
              >
                Cancelar
              </button>
            </div>
            <div className="col">
              <button type="submit" className="btn">
                Login
              </button>
            </div>
            <div className="col">
              <button
                type="submit"
                className="btn"
                onClick={() => history.push('/user/register')}
              >
                Registrarse
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};

export default Login;
