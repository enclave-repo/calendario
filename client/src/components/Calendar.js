import { useEffect, useState } from 'react';
import { useQuery } from 'react-query';
import axios from 'axios';
import dayjs from 'dayjs';

import { Week } from '.';
import { Fragment } from 'react';
import { useHistory } from 'react-router-dom';

const Calendar = () => {
  const [user, setUser] = useState(null);

  const { isLoading, isSuccess, data: loggedUser } = useQuery(
    'user',
    async () => (await axios.get('/_back/api/users/auth')).data
  );

  useEffect(() => {
    if (!isLoading && isSuccess) setUser(loggedUser?.name ?? null);
  }, [isLoading, isSuccess, loggedUser]);

  const { error, data } = useQuery('events', () =>
    axios.get('/_back/api/events')
  );

  let calendarContent = 'Loading...';

  if (error) calendarContent = 'Something went wrong, please try again.';

  const now = dayjs();
  const oneweekfromnow = now.add(7, 'days');

  let weeks = [[], []];

  (data?.data ?? [])
    .map((event) => ({
      ...event,
      date: new Date(event.date)
    }))
    .forEach((event) => {
      if (event.date < oneweekfromnow.toDate()) weeks[0].push(event);
      else weeks[1].push(event);
    });

  calendarContent = weeks.map((events, index) => (
    <div id={'week_' + index} key={index}>
      <Week now={now} events={events} user={user} />
    </div>
  ));

  const history = useHistory();

  const onClick = () => {
    history.push('/events/new');
  };

  return (
    <Fragment>
      <div className="container">
        <div className="calendar">{calendarContent[0]}</div>
        <hr />
        <div className="columns">
          {!user ? (
            ''
          ) : (
            <div className="col">
              <button className="btn" onClick={onClick}>
                Añadir al Calendario
              </button>
            </div>
          )}
          {user ? (
            <div className="col-3">
              <button
                className="btn"
                onClick={() => {
                  localStorage.removeItem('_cal_token');
                  window.location.reload();
                }}
              >
                Logout
              </button>
            </div>
          ) : (
            <div className={`col${!user ? '' : '-3'}`}>
              <button
                className="btn"
                onClick={() => history.push('/user/login')}
              >
                Login
              </button>
            </div>
          )}
        </div>
        <hr />
      </div>
    </Fragment>
  );
};

export default Calendar;
