import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useQuery, useMutation } from 'react-query';

import axios from 'axios';

const NewEvent = () => {
  const history = useHistory();
  const token = localStorage.getItem('_cal_token');

  if (token) axios.defaults.headers.common['x-auth-token'] = token;
  else delete axios.defaults.headers.common['x-auth-token'];

  const { isLoading, isError, isSuccess: logged } = useQuery(
    'user',
    () => axios.get('/_back/api/users/auth'),
    { enabled: !token }
  );

  useEffect(() => {
    if (!isLoading && isError) {
      delete axios.defaults.headers.common['x-auth-token'];
      localStorage.removeItem('_cal_token');
      history.push('/user/login');
    }
  }, [isLoading, isError, history]);

  const [formData, setData] = useState({
    name: '',
    password: '',
    time: '',
    type: '',
    user: ''
  });
  const [responseMessage, setResponseMessage] = useState('');
  const [spanResponseClass, setSpanResponseClass] = useState('');

  const { name, date, time, type, user } = formData;

  const {
    isLoading: isSending,
    mutate,
    error,
    isSuccess,
    data
  } = useMutation((event) => axios.post('/_back/api/events', event));

  const onChange = (e) =>
    setData(() => ({ ...formData, [e.target.name]: e.target.value }));

  const onSubmit = (e) => {
    e.preventDefault();
    const datetime = new Date(date + ' ' + time);

    mutate({ name, date: datetime, type, user });
  };

  useEffect(() => {
    if (!isSending) {
      setResponseMessage(error?.response.data ?? '');
      setSpanResponseClass('warning');
    }
  }, [isSending, error?.response.data]);

  useEffect(() => {
    if (isSuccess) {
      setResponseMessage('Evento añadido. Redirigiendo al calendario...');
      setSpanResponseClass('success');

      setTimeout(() => {
        history.push('/');
      }, 3000);
    }
  }, [isSuccess, data, history]);

  return !isLoading && logged ? (
    <div className="container">
      <form className="form centered" onSubmit={(e) => onSubmit(e)}>
        <div className="form-group">
          <div className="columns">
            <div className="col-2">
              <h4>Nombre</h4>
            </div>
            <div className="col-8">
              <input type="text" name="name" value={name} onChange={onChange} />
            </div>
          </div>
        </div>

        <div className="form-group">
          <div className="columns">
            <div className="col-2">
              <h4>Día</h4>
            </div>
            <div className="col-3">
              <input type="date" name="date" onChange={onChange} />
            </div>
            <div className="col-2">
              <h4>Hora</h4>
            </div>
            <div className="col-3">
              <input type="time" name="time" onChange={onChange} />
            </div>
          </div>
        </div>

        <div className="form-group">
          <div className="columns">
            <div className="col-2">
              <h4>Tipo</h4>
            </div>
            <div className="col-8">
              <select name="type" value={type} onChange={onChange}>
                <option value="">Seleccionar...</option>
                {[
                  { text: 'Película', type: 'movie' },
                  { text: 'Serie', type: 'serie' },
                  { text: 'Votación', type: 'voting' },
                  { text: 'Otro', type: 'other' }
                ].map((option) => (
                  <option value={option.type} key={option}>
                    {option.text}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>

        <div className="columns">
          <div className="col-2"></div>
          <div className="col-8">
            <span className={spanResponseClass}>{responseMessage}</span>
          </div>
        </div>

        <div className="form-group">
          <div className="columns">
            <div className="col-5">
              <button
                type="button"
                className="btn"
                onClick={() => history.goBack()}
              >
                Cancelar
              </button>
            </div>
            <div className="col-5">
              <button type="submit" className="btn">
                Guardar
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  ) : (
    ''
  );
};

export default NewEvent;
