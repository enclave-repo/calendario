import { useEffect, useState } from 'react';
import { useMutation } from 'react-query';
import { useHistory } from 'react-router-dom';

import axios from 'axios';

const Registration = () => {
  const history = useHistory();

  const [formData, setData] = useState({ name: '', password: '' });
  const [responseMessage, setResponseMessage] = useState('');
  const [spanResponseClass, setSpanResponseClass] = useState('');

  const { name, password } = formData;

  const { isLoading, mutate, error, isSuccess } = useMutation((newUser) =>
    axios.post('/_back/api/users', newUser)
  );

  const onChange = (e) =>
    setData(() => ({ ...formData, [e.target.name]: e.target.value }));

  const onSubmit = (e) => {
    e.preventDefault();
    mutate({ name, password });
  };

  useEffect(() => {
    if (!isLoading) {
      setResponseMessage(error?.response.data ?? '');
      setSpanResponseClass('warning');
    }
  }, [isLoading, error?.response.data]);

  useEffect(() => {
    if (isSuccess) {
      setResponseMessage('Registrado. Redirigiendo a página de login...');
      setSpanResponseClass('success');

      setTimeout(() => {
        history.push('/user/login');
      }, 3000);
    }
  }, [isSuccess, history]);

  return (
    <div className="container">
      <form className="form centered" onSubmit={(e) => onSubmit(e)}>
        <div className="form-group">
          <div className="columns">
            <div className="col-2">
              <h4>Usuario</h4>
            </div>
            <div className="col-8">
              <input type="text" name="name" value={name} onChange={onChange} />
            </div>
          </div>
        </div>

        <div className="form-group">
          <div className="columns">
            <div className="col-2">
              <h4>Contraseña</h4>
            </div>
            <div className="col-8">
              <input
                type="password"
                name="password"
                value={password}
                onChange={onChange}
              />
            </div>
          </div>
        </div>

        <div className="columns">
          <div className="col-2"></div>
          <div className="col-8">
            <span className={spanResponseClass}>{responseMessage}</span>
          </div>
        </div>

        <div className="form-group">
          <div className="columns">
            <div className="col-5">
              <button
                type="button"
                className="btn"
                onClick={() => history.push('/')}
              >
                Cancelar
              </button>
            </div>
            <div className="col-5">
              <button type="submit" className="btn">
                Registrarse
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};

export default Registration;
