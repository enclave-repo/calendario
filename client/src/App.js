import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { useQuery } from 'react-query';
import axios from 'axios';

import { Calendar, Registration, Login, NewEvent } from './components';

import './App.css';

const App = () => {
  const token = localStorage.getItem('_cal_token');

  if (token) axios.defaults.headers.common['x-auth-token'] = token;
  else delete axios.defaults.headers.common['x-auth-token'];

  const { isLoading, isError } = useQuery(
    'user',
    () => axios.get('/_back/api/users/auth'),
    { enabled: !!token }
  );

  if (!isLoading && isError) {
    delete axios.defaults.headers.common['x-auth-token'];
    localStorage.removeItem('_cal_token');
  }

  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Calendar} />
        <Route path="/user/register" component={Registration} />
        <Route path="/user/login" component={Login} />
        <Route path="/events/new" component={NewEvent} />
      </Switch>
    </Router>
  );
};

export default App;
