'use strict';

const express = require('express');
const compression = require('compression');
const path = require('path');

const DB = require('./libraries/db');
const routes = require('./libraries/routes');

const app = express();

DB.init();

app.use(express.json(), compression(), express.static(__dirname + '/public'));

routes(app, DB.db);

app.set('trust proxy', process.env.APP_TRUSTPROXY === 'true');

if (process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, 'client', 'build')));
  app.get('/*', (req, res) => {
    res.sendFile(path.resolve('client', 'build', 'index.html'));
  });
}

module.exports = app;
