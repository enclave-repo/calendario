'use strict';

const dayjs = require('dayjs');

const { Events, Users } = require('../models');

const auth = require('../middleware/auth');

const routes = (app, db) => {
  app.post('/_back/api/users', async (req, res) => {
    try {
      const User = new Users(db);
      const user = await User.save(req.body);

      if (user.error) return res.status(400).send(user.error);
      res.json(user);
    } catch (error) {
      console.error(error);
      res.status(500).send('Server Error');
    }
  });

  app.post('/_back/api/users/login', async (req, res) => {
    try {
      const User = new Users(db);
      const token = await User.login(req.body);

      if (token.error) return res.status(400).send(token.error);
      res.json(token);
    } catch (error) {
      console.error(error);
      res.status(500).send('Server Error');
    }
  });

  app.get('/_back/api/users/auth', auth, async (req, res) => {
    try {
      const User = new Users(db);
      const user = await User.auth(req.user);

      if (user.error) return res.status(400).send(user.error);

      res.json(user);
    } catch (error) {
      console.error(error);
      res.status(500).send('Server Error');
    }
  });

  app.post('/_back/api/events', auth, async (req, res) => {
    try {
      const Event = new Events(db);
      const event = await Event.save({ ...req.body, user: req.user });

      if (event.error) return res.status(400).send(event.error);
      res.json(event);
    } catch (error) {
      console.error(error);
      res.status(500).send('Server Error');
    }
  });

  app.get('/_back/api/events', async (req, res) => {
    const now = dayjs();
    const startdate = now.subtract(2, 'hour');
    const twoweeks = now.add(14, 'days');

    try {
      const Event = new Events(db);
      const User = new Users(db);

      let events = await Event.get({
        date: {
          $lte: twoweeks.unix() * 1000, //toDate(),
          $gte: startdate.unix() * 1000 //toDate()
        }
      });

      events = await Promise.all(
        events.map(async (event) => ({
          ...event,
          user: (await User.getOne({ _id: event.user }))?.name ?? ''
        }))
      );

      res.json(events);
    } catch (error) {
      console.error(error);
      res.status(500).send('Server Error');
    }
  });

  app.delete('/_back/api/events/:id', auth, async (req, res) => {
    try {
      const Event = new Events(db);
      const deleted = await Event.delete(req.params.id);

      if (deleted.numRemoved === 0) return res.status(400).send("Couln't delete, please try again");

      res.json({ success: true });
    } catch (error) {
      console.error(error);
      res.status(500).send('Server Error');
    }
  });
};

module.exports = routes;
