'use strict';

const Datastore = require('nedb');
const util = require('util');

const Models = require('../models');

function DB() {
  this.db = null;
}

DB.prototype.init = function () {
  if (Object.keys(Models).length > 0) {
    let db = {};
    for (const Model in Models) {
      db[Model] = new Datastore({ filename: './datastore/' + Model + '.db', autoload: true });

      db[Model].insert = util.promisify(db[Model].insert).bind(db[Model]);
      db[Model].find = util.promisify(db[Model].find).bind(db[Model]);
      db[Model].findOne = util.promisify(db[Model].findOne).bind(db[Model]);
      db[Model].remove = util.promisify(db[Model].remove).bind(db[Model]);
    }

    this.db = db;
  }

  if (this.db === null) throw new Error('No db loaded');
  else console.log('Databases loaded successfully');
};

module.exports = new DB();
