'use strict';

const jwt = require('jsonwebtoken');

const set = (user) => {
  const payload = { user: user._id };

  return jwt.sign(payload, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRATION
  });
};

const decode = (token) => {
  const decoded = jwt.verify(token, process.env.JWT_SECRET);

  return decoded.user;
};

module.exports = {
  set,
  decode
};
